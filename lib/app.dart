import 'package:flutter/material.dart';
import 'package:xylophone_app/xylophone.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Display(
        title: 'Xylophone App',
      ),
    );
  }
}

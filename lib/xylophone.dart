// ignore: import_of_legacy_library_into_null_safe
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class Display extends StatefulWidget {
  const Display({super.key, required this.title});

  final String title;

  @override
  State<Display> createState() => _Display();
}

Widget sounds(int index, Color color) {
  final player = AudioPlayer();

  return Expanded(
    child: TextButton(
      onPressed: () {
        // final player = AudioPlayer();
        player.play(AssetSource('note$index.wav'));
      },
      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(color)),
      child: const Text(''),
    ),
  );
}

class _Display extends State<Display> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          sounds(1, Colors.red),
          sounds(2, Colors.orange),
          sounds(3, Colors.yellow),
          sounds(4, Colors.green),
          sounds(5, Colors.teal),
          sounds(6, Colors.blue),
          sounds(7, Colors.purple),
        ],
      ),
    );
  }
}
